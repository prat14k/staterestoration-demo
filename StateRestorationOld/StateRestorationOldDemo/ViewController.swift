//
//  ViewController.swift
//  StateRestorationDemo
//
//  Created by Prateek Sharma on 6/11/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var savedText: String?
    @IBOutlet weak private var textField: UITextField!
    
    override func encodeRestorableState(with coder: NSCoder) {
        coder.encode(textField.text, forKey: "Text")
        super.encodeRestorableState(with: coder)
    }
    override func decodeRestorableState(with coder: NSCoder) {
        savedText = coder.decodeObject(forKey: "Text") as? String
        savedText?.removeLast(15)
        super.decodeRestorableState(with: coder)
    }
    override func applicationFinishedRestoringState() {
        textField.text = savedText
        super.applicationFinishedRestoringState()
    }    
}
