//
//  ViewController.swift
//  StateRestorationDemo
//
//  Created by Prateek Sharma on 6/11/19.
//  Copyright © 2019 Prateek Sharma. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction private func languageUpdate() {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
    
    private let myUserActivity: NSUserActivity = {
        let userActivity = NSUserActivity(activityType: "StateRestoreDemo")
        userActivity.userInfo = ["Data": "iOS 13 State Restoration"]
        return userActivity
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.window?.windowScene?.userActivity = myUserActivity
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.window?.windowScene?.userActivity = nil
    }
}

